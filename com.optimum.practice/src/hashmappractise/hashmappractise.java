package hashmappractise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class hashmappractise {
	
	static String name = "", mobile = "";  	// key
	static Long phoneNumber = 0L;	// value
	
	public static int mainMenu() throws NumberFormatException, IOException
	{
		BufferedReader refBufferedReader = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("1. Enter Phone Entries : ");
		System.out.println("2. lookup in the Phone Book : ");
		System.out.println("3. Display name in the Book : ");
		System.out.println("4. Exit");

		System.out.println("Enter your choice : ");
		int choice = Integer.parseInt(refBufferedReader.readLine());
		return choice;
	}
	
	public static void menu(int choice, BufferedReader refBufferedReader) throws IOException
	{
		refBufferedReader = new BufferedReader(new InputStreamReader(System.in));
		switch(choice)
		{
		case 1:
			
			System.out.println("Enter Name : ");
			name = refBufferedReader.readLine();

			System.out.println("Enter Mobile Number : ");
			phoneNumber = Long.parseLong(refBufferedReader.readLine());
			break;
			
		case 2:
			System.out.println("Enter Name : ");
			name = refBufferedReader.readLine();
			name = name.trim(); // remove unwanted spaces
			break;
		}
		
	}
	
	
	public static boolean addPhoneEntries(Map<String, Long> refHashmap, String name, Long mobile)
	{
		refHashmap.put(name, mobile);
		return true;
	}

	public static boolean getPhoneNumber(Map<String, Long> refHashmap, String name, Long phoneNumber)
	{
		phoneNumber = refHashmap.get(name);
		return true;
	}
	
	public static void displayData(Map<String, Long> refHashmap)
	{
		Set<String> refSet = new HashSet<String>();
		
		boolean test2 = refHashmap.isEmpty();
		refSet = refHashmap.keySet();

		refHashmap.forEach((key,value)->System.out.println(key + " "+ value));
		//System.out.println(refHashmap.keySet() + "  "+refHashmap.values());
	}
	
	public static void main(String[] args)throws NumberFormatException, IOException {

		Map<String,Long> refHashMap = new HashMap<String,Long>();
		BufferedReader refBufferedReader = null;

	
		boolean appLoop = true;

		while(appLoop) {
			
			int choice = mainMenu();
			
			switch (choice) {
			case 1:

				menu(1, refBufferedReader);
				System.out.println(name + " " + phoneNumber);
				addPhoneEntries(refHashMap, name, phoneNumber);
				
				break;

			case 2:

				menu(2, refBufferedReader);
				getPhoneNumber(refHashMap, name, phoneNumber);

				System.out.println("Phone Number "+phoneNumber);

				break;

			case 3:
				displayData(refHashMap);
				break;
				
			case 4:
				appLoop = false;
				break;
			}
		}

	}

}
