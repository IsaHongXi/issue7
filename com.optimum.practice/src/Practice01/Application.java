package Practice01;

import java.util.Scanner;

public class Application {
	
	public enum MenuState{
		
		MAINMENU,
		OPTIONS,
		EXIT;
		
		public void MenuDisplay(MenuState refMenu)
		{
			switch(refMenu)
			{
			case MAINMENU:
				System.out.println(" ");
				System.out.println("Welcome");
				System.out.println("Menu Screen:");
				System.out.println("1. Options");
				System.out.println("2. Exit");
				break;
				
			case OPTIONS:
				System.out.println(" ");
				System.out.println("Options Page");
				System.out.println("Menu Screen:");
				System.out.println("1. Back");				
				break;
				
			case EXIT:
				System.out.println(" ");
				System.out.println("Goodbye and have a nice day!");
				break;
				
			}
		}
		
		public MenuState ChangeMenu(int input, MenuState refMenu)
		{
			switch(input)
			{
			case 1:
				switch (refMenu)
				{
				case MAINMENU:
					refMenu = MenuState.OPTIONS;
					
					break;
					
				case OPTIONS:
					refMenu = MenuState.MAINMENU;
					break;
				}
				break;
			case 2:
				switch (refMenu)
				{
				case MAINMENU:
					refMenu = MenuState.EXIT;
					break;
				}
				break;
			}
			
			return refMenu;
		}
		
	}
	
	public void Update(MenuState refMenu)
	{
		int input = 0;
		Scanner refScanner = new Scanner(System.in);
		
		
		while(refMenu != MenuState.EXIT)
		{
			System.out.println(refMenu);
			refMenu.MenuDisplay(refMenu);
			System.out.println(" ");
			System.out.print("Enter your option: ");
			input = Integer.parseInt(refScanner.nextLine());
			refMenu = refMenu.ChangeMenu(input, refMenu);
			
			
			
		}
		
		refScanner.close();
		
	}

	public static void main(String[] args) {
		
		Application refApp = new Application();
		MenuState refMenu = MenuState.MAINMENU;
		
		refApp.Update(refMenu);

	}

}
