package issue7;

import java.util.Scanner;

public class Service {

	Pojo refPojo = new Pojo();
	Dao refD = new Dao();
	Scanner refScanner;

	public void addOrderService()
	{
		refScanner = new Scanner(System.in);

		System.out.println("Please Enter ID: ");
		var input = Integer.parseInt(refScanner.nextLine());
		System.out.println("Please Enter Name: ");
		var input2 = refScanner.nextLine();

		refPojo.setProductID(input);
		refPojo.setProductName(input2);
		refD.addOrder(refPojo);

	}

	public void updateOrderService()
	{
		refScanner = new Scanner(System.in);

		System.out.println("Please Enter ID: ");
		var input = Integer.parseInt(refScanner.nextLine());
		System.out.println("Please Enter Name: ");
		var input2 = refScanner.nextLine();

		refPojo.setProductID(input);
		refPojo.setProductName(input2);
		refD.updateOrder(refPojo);
	}

	public void deleteOrderService()
	{
		refScanner = new Scanner(System.in);

		System.out.println("Please Enter ID: ");
		var input = Integer.parseInt(refScanner.nextLine());

		refPojo.setProductID(input);
		refD.deleteOrder(refPojo);
	}

	public void displayOrderHistoryService()
	{
		refD.getOrderHistory();
	}

	public void menu()
	{
		refScanner = new Scanner(System.in);
		System.out.println("1. Add order");
		System.out.println("2. Update order");
		System.out.println("3. Delete order");
		System.out.println("4. Display order history");

		var choice = Integer.parseInt(refScanner.nextLine());

		switch(choice)
		{
		case 1:
			addOrderService();
			refD.closeConnection();
			break;
		case 2:
			updateOrderService();
			refD.closeConnection();
			break;
		case 3:
			deleteOrderService();
			refD.closeConnection();
			break;
		case 4:
			displayOrderHistoryService();
			refD.closeConnection();
			break;
		}

	}

}
