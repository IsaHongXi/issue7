package issue7;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



public class Dao {
	
	Connection refConnection = null;
	PreparedStatement refPreparedStatement = null;
	
	public boolean closeConnection()
	{
		try {
			refConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean getOrderHistory()
	{
		try {
			refConnection = DBUtil.getConnection();
			
			String sqlQuery = "select * from order_audit";
		
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
			ResultSet rs = refPreparedStatement.executeQuery(); 
			System.out.println("Product ID | Product Name  | action");
			while(rs.next())
			{
				System.out.println(rs.getString(1) + "\t\t" + rs.getString(2) + "\t\t" + rs.getString(3));
			}
			
			
		}
		catch(Exception e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	
	}
	
	public boolean addOrder(Pojo refPojo) {
		
		try {
			refConnection = DBUtil.getConnection();
			
			String sqlQuery = "insert into orders(productID, productName) values(?,?)";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1, refPojo.getProductID());
			refPreparedStatement.setString(2, refPojo.getProductName());
						
			refPreparedStatement.execute();  
			System.out.println("Inserted record successfully");		
			
			
		}
		catch(SQLException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		
		return true;
		
	}
	
	public boolean updateOrder(Pojo refPojo)
	{
		try {
			refConnection = DBUtil.getConnection();
			
			String sqlQuery = "update orders set productName=? where productID=?";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(2, refPojo.getProductID());
			refPreparedStatement.setString(1, refPojo.getProductName());
						
			refPreparedStatement.execute();  
			System.out.println("Update record successfully");		
			
			
		}
		catch(SQLException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	}
	
	public boolean deleteOrder(Pojo refPojo)
	{
		try {
			refConnection = DBUtil.getConnection();
			
			String sqlQuery = "delete from orders where productID=?";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1, refPojo.getProductID());
						
			refPreparedStatement.execute();  
			System.out.println("Delete record successfully");		
			
			
		}
		catch(SQLException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		
		return true;
	}

}
