package issue7;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DaoTest {
	
	Dao refDao = new Dao();
	Pojo refPojo = new Pojo();

	@Test
	public void testGetOrderHistory() {
		
		assertTrue(refDao.getOrderHistory());
		
	}

	@Test
	public void testAddOrder() {
		
		refPojo.setProductID(123);
		refPojo.setProductName("testcase1");
		assertTrue(refDao.addOrder(refPojo));
		
	}

	@Test
	public void testUpdateOrder() {
		
		refPojo.setProductID(5);
		refPojo.setProductName("pencil");
		refDao.addOrder(refPojo);
		refPojo.setProductID(5);
		refPojo.setProductName("testcase3");
		assertTrue(refDao.updateOrder(refPojo));
		
	}

	@Test
	public void testDeleteOrder() {
		
		refPojo.setProductID(4);
		refPojo.setProductName("testcasedelete");
		refDao.addOrder(refPojo);
		refPojo.setProductID(4);
		assertTrue(refDao.deleteOrder(refPojo));
		//refDao.closeConnection();
		
	}

}
