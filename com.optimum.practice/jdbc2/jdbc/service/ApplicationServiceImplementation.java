package jdbc.service;

import java.util.Scanner;

import jdbc.dao.*;
import jdbc.pojo.*;

public class ApplicationServiceImplementation implements ApplicationService {

	ApplicationDAO refApplicationDAO;
	Scanner refScanner;
	User refUser;
	
	@Override
	public void userInputInsertRecord() {
		
		refScanner = new Scanner(System.in);
		
		System.out.println("Enter User ID : ");
		int userLoginID = refScanner.nextInt();
		
		System.out.println("Enter User Password : ");
		String userPassword = refScanner.next();
		
		refUser = new User();
		refUser.setUserID(userLoginID);
		refUser.setUserPassword(userPassword);
		
		refApplicationDAO = new ApplicationDAOImplementation();
		refApplicationDAO.insertRecord(refUser);

	}

	@Override
	public void userChoice() {
		
		System.out.println("Enter Choice");
		refScanner = new Scanner(System.in);
		int choice = refScanner.nextInt();
		switch (choice) {
		case 1:
			userInputInsertRecord();
			break;

		default:
			System.out.println("Option not found..");
			break;
		} // end of userChoice

	}

}
