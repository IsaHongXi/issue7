package jdbc.dao;

import jdbc.pojo.User;

public interface ApplicationDAO {

	void getUserRecord();
	void insertRecord(User refUser);
	void deleteRecord();
	void updateRecord();
}
