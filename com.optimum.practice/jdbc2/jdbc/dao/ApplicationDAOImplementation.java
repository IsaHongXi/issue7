package jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import jdbc.pojo.User;
import utility.DBUtility;

public class ApplicationDAOImplementation implements ApplicationDAO {
	
	Connection refConnection = null;
	PreparedStatement refPreparedStatement = null;

	@Override
	public void getUserRecord() {
		

	}


	@Override
	public void insertRecord(User refUser) {
		
		try {
			refConnection = DBUtility.getConnection();
			
			//mysql inser query
			String sqlQuery = "insert into user(user_id, user_password) values(?,?)";
			
			// create the mysql insert preparedstatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1, refUser.getUserID());
			refPreparedStatement.setString(2, refUser.getUserPassword());
						
			refPreparedStatement.execute();  
			System.out.println("Inserted record successfully");		
			
			
		}
		catch(SQLException e) {
			
			System.out.println("Exception Handled while insert record");
			
		}
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
		
	}

	@Override
	public void deleteRecord() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateRecord() {
		// TODO Auto-generated method stub
		
	}


}
